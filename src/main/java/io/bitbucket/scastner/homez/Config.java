package io.bitbucket.scastner.homez;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.plugin.Plugin;

public class Config {
    // HomeZ
    public static String chatPrefix;
    public static long defaultMaxAmount;
    public static long vipMaxAmount;
    // Economy
    public static boolean economyEnabled;
    public static double defaultCost;
    public static double vipCost;
    // Messages
    public static String setHomeSuccess;
    public static String setHomeFailed;
    public static String delHomeSuccess;
    public static String delHomeFailed;
    public static String teleportedHome;
    public static String homeNotFound;
    public static String toManyHomes;
    public static String multipleHomesFound;
    public static String economyWithdrawSuccess;
    public static String economyWithdrawFailed;

    public static void init(Plugin plugin) {
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();
        // HomeZ
        chatPrefix = translateString(plugin, "HomeZ.Prefix");
        defaultMaxAmount = plugin.getConfig().getLong("HomeZ.MaxAmount.Default");
        vipMaxAmount = plugin.getConfig().getLong("HomeZ.MaxAmount.VIP");
        // Economy
        economyEnabled = plugin.getConfig().getBoolean("Economy.Enabled");
        defaultCost = plugin.getConfig().getDouble("Economy.Cost.Default");
        vipCost = plugin.getConfig().getDouble("Economy.Cost.VIP");
        // Messages
        setHomeSuccess = translateString(plugin, "Messages.SetHomeSuccess");
        setHomeFailed = translateString(plugin, "Messages.SetHomeFailed");
        delHomeSuccess = translateString(plugin, "Messages.DelHomeSuccess");
        delHomeFailed = translateString(plugin, "Messages.DelHomeFailed");
        teleportedHome = translateString(plugin, "Messages.TeleportedHome");
        homeNotFound = translateString(plugin, "Messages.HomeNotFound");
        toManyHomes = translateString(plugin, "Messages.ToManyHomes");
        multipleHomesFound = translateString(plugin, "Messages.MultipleHomesFound");
        economyWithdrawSuccess = translateString(plugin, "Messages.EconomyWithdrawSuccess");
        economyWithdrawFailed = translateString(plugin, "Messages.EconomyWithdrawFailed");
    }

    private static String translateString(Plugin plugin, String path) {
        return ChatColor.translateAlternateColorCodes('&', "&r" + plugin.getConfig().getString(path));
    }
}
