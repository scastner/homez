package io.bitbucket.scastner.homez.commands;

import io.bitbucket.scastner.homez.Config;
import io.bitbucket.scastner.homez.Database;
import io.bitbucket.scastner.homez.HomeZ;
import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Home implements CommandExecutor, TabCompleter {
    private HomeZ plugin;

    public Home(HomeZ plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 1) {
                Location location = Database.getHome(player, args[0]);
                if (location != null) {
                    Economy economy = plugin.getEconomy();
                    if (economy != null && Config.economyEnabled) {
                        if (!player.hasPermission("homez.free")) {
                            double cost = Config.defaultCost;
                            if (player.hasPermission("homez.vip")) {
                                cost = Config.vipCost;
                            }

                            EconomyResponse response = economy.withdrawPlayer(player, cost);
                            if (response.transactionSuccess()) {
                                player.sendMessage(Config.chatPrefix + " " + Config.economyWithdrawSuccess
                                    .replace("{cost}", String.valueOf(cost))
                                    .replace("{balance}", String.valueOf(economy.getBalance(player)))
                                );
                            } else {
                                player.sendMessage(Config.chatPrefix + " " + Config.economyWithdrawFailed
                                    .replace("{cost}", String.valueOf(cost))
                                    .replace("{balance}", String.valueOf(economy.getBalance(player)))
                                );
                                return true;
                            }
                        }
                    }

                    player.setFallDistance(0);
                    player.setVelocity(new Vector());
                    player.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
                    player.sendMessage(Config.chatPrefix + " " + Config.teleportedHome.replace("{home}", args[0]));
                } else {
                    player.sendMessage(Config.chatPrefix + " " + Config.homeNotFound.replace("{home}", args[0]));
                }
            } else {
                player.sendMessage(Config.chatPrefix + " " + command.getUsage());
            }
        } else if (sender != null) {
            sender.sendMessage(String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> list = new ArrayList<>();
        if (sender instanceof Player) {
            Player player = (Player) sender;
            list = Database.getHomes(player);
        }
        return list;
    }
}
