package io.bitbucket.scastner.homez.commands;

import io.bitbucket.scastner.homez.Config;
import io.bitbucket.scastner.homez.Database;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class SetHome implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 1) {
                List<String> homes = Database.getHomes(player);
                long max = Config.defaultMaxAmount;
                if (player.hasPermission("homez.vip")) {
                    max = Config.vipMaxAmount;
                }

                if (homes.size() < max) {
                    boolean success = Database.saveHome(player, args[0]);
                    if (success) {
                        player.sendMessage(Config.chatPrefix + " " + Config.setHomeSuccess.replace("{home}", args[0]));
                    } else {
                        player.sendMessage(Config.chatPrefix + " " + Config.setHomeFailed.replace("{home}", args[0]));
                    }
                } else {
                    player.sendMessage(Config.chatPrefix + " " + Config.toManyHomes.replace("{max}", String.valueOf(max)));
                }
            } else {
                player.sendMessage(Config.chatPrefix + " " + command.getUsage());
            }
        } else if (sender != null) {
            sender.sendMessage(String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }
}
