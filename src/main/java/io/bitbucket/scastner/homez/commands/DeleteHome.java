package io.bitbucket.scastner.homez.commands;

import io.bitbucket.scastner.homez.Config;
import io.bitbucket.scastner.homez.Database;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class DeleteHome implements CommandExecutor, TabCompleter {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 1) {
                boolean success = Database.deleteHome(player, args[0]);
                if (success) {
                    player.sendMessage(Config.chatPrefix + " " + Config.delHomeSuccess.replace("{home}", args[0]));
                } else {
                    player.sendMessage(Config.chatPrefix + " " + Config.delHomeFailed.replace("{home}", args[0]));
                }
            } else {
                player.sendMessage(Config.chatPrefix + " " + command.getUsage());
            }
        } else if (sender != null) {
            sender.sendMessage(String.format("%sThis command is only usable by players", ChatColor.RED));
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> list = new ArrayList<>();
        if (sender instanceof Player) {
            Player player = (Player) sender;
            list = Database.getHomes(player);
        }
        return list;
    }
}
