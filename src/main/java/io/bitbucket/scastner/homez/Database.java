package io.bitbucket.scastner.homez;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Database {
    private static JavaPlugin plugin;
    private static Connection connection;

    private static final String sqlHomeTable = "" +
        "CREATE TABLE IF NOT EXISTS home (" +
        "   player_id TEXT NOT NULL," +
        "   name TEXT NOT NULL," +
        "   world TEXT NOT NULL," +
        "   x REAL NOT NULL," +
        "   y REAL NOT NULL," +
        "   z REAL NOT NULL," +
        "   PRIMARY KEY(player_id, name)" +
        ") WITHOUT ROWID";
    private static final String sqlSaveHome = "" +
        "INSERT OR REPLACE INTO home(player_id, name, world, x, y, z) " +
        "VALUES(?, ?, ?, ?, ?, ?)";
    private static final String sqlGetHome = "" +
        "SELECT world, x, y, z FROM home WHERE player_id = ? AND name = ?";
    private static final String sqlGetHomes = "" +
        "SELECT name FROM home WHERE player_id = ?";
    private static final String sqlDeleteHome = "" +
        "DELETE FROM home WHERE player_id = ? AND name = ?";

    public static void init(JavaPlugin plugin) {
        try {
            Database.plugin = plugin;
            if (!plugin.getDataFolder().exists()) {
                if (plugin.getDataFolder().mkdir()) {
                    new File(plugin.getDataFolder(), "homez.db");
                }
            }
            connection = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder().getAbsolutePath() + "/homez.db");
            createHomeTable();
        } catch (SQLException e) {
            logException("Open database", e);
        }
    }

    public static void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            logException("Close database", e);
        }
    }

    public static boolean saveHome(Player player, String name) {
        if (connection != null) {
            try (PreparedStatement stmt = connection.prepareStatement(sqlSaveHome)) {
                stmt.setString(1, player.getUniqueId().toString());
                stmt.setString(2, name);
                stmt.setString(3, player.getWorld().getUID().toString());
                stmt.setDouble(4, player.getLocation().getX());
                stmt.setDouble(5, player.getLocation().getY());
                stmt.setDouble(6, player.getLocation().getZ());
                stmt.executeUpdate();
                return true;
            } catch (SQLException e) {
                logException("saveHome", e);
            }
        }
        return false;
    }

    public static Location getHome(Player player, String name) {
        if (connection != null) {
            try (PreparedStatement stmt = connection.prepareStatement(sqlGetHome)) {
                stmt.setString(1, player.getUniqueId().toString());
                stmt.setString(2, name);
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    return new Location(
                        player.getServer().getWorld(UUID.fromString(rs.getString(1))),
                        rs.getDouble(2),
                        rs.getDouble(3),
                        rs.getDouble(4)
                    );
                }
            } catch (SQLException e) {
                logException("getHome", e);
            }
        }
        return null;
    }

    public static List<String> getHomes(Player player) {
        List<String> homes = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement(sqlGetHomes)) {
            stmt.setString(1, player.getUniqueId().toString());
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                homes.add(rs.getString(1));
            }
        } catch (SQLException e) {
            logException("getHomes", e);
        }

        return homes;
    }

    public static boolean deleteHome(Player player, String name) {
        if (connection != null) {
            try (PreparedStatement stmt = connection.prepareStatement(sqlDeleteHome)) {
                stmt.setString(1, player.getUniqueId().toString());
                stmt.setString(2, name);
                stmt.executeUpdate();
                return true;
            } catch (SQLException e) {
                logException("deleteHome", e);
            }
        }
        return false;
    }

    private static void createHomeTable() {
        if (connection != null) {
            try (Statement stmt = connection.createStatement()) {
                stmt.executeUpdate(sqlHomeTable);
            } catch (SQLException e) {
                logException("createHomeTable", e);
            }
        }
    }

    private static void logException(String message, Exception e) {
        plugin.getLogger().severe(message + ":\n" + String.format("%s" + e.getMessage(), ChatColor.RED));
    }
}
