package io.bitbucket.scastner.homez;

import io.bitbucket.scastner.homez.commands.DeleteHome;
import io.bitbucket.scastner.homez.commands.Home;
import io.bitbucket.scastner.homez.commands.SetHome;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class HomeZ extends JavaPlugin {
    private Economy economy = null;

    @Override
    public void onEnable() {
        Config.init(this);
        Database.init(this);

        getCommand("sethome").setExecutor(new SetHome());
        getCommand("delhome").setExecutor(new DeleteHome());
        getCommand("home").setExecutor(new Home(this));

        getCommand("home").setTabCompleter(new Home(this));

        if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> provider = getServer().getServicesManager().getRegistration(Economy.class);
            if (provider != null) {
                economy = provider.getProvider();
            }
        }
    }

    @Override
    public void onDisable() {
        Database.close();
    }

    public Economy getEconomy() {
        return economy;
    }
}
